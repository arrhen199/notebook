﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Notebook
{
    /// <summary>
    /// Interaction logic for Document.xaml
    /// </summary>
    /// 

    public partial class DocumentFilm : Window
    {
        public Film film { get; set; }
        public DocumentFilm()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < film.MainActors.Count; i++ )
                Actors.Text += film.MainActors[i]+"\n";
        }

        private void CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            PrintDialog printDialog =new  PrintDialog();
            if(printDialog.ShowDialog() == true)
            {
                printDialog.PrintVisual(GridPrint, "Drukowanie");
            }
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
