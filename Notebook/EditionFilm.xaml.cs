﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Notebook
{
    /// <summary>
    /// Interaction logic for EditionFilm.xaml
    /// </summary>
    public partial class EditionFilm : Window
    {
        public Film film { get; set; }
        public Film filmbinding{get; set;}
        private bool IsNewFilm = false;

        private static RoutedUICommand COMMAND_VAR;
        private static RoutedUICommand COMMAND_OK_VAR;

        public EditionFilm()
        {
            COMMAND_VAR = new RoutedUICommand("n1", "n2", typeof(EditionFilm));
            COMMAND_OK_VAR = new RoutedUICommand("n3", "n4", typeof(EditionFilm));
            InitializeComponent();
        }

        private void Loadedd(object sender, RoutedEventArgs e)
        {
            BindingExpression be;
            if (film == null)
            {
                filmbinding = new Film();
                IsNewFilm = true;
            }
            else
            {
                filmbinding = new Film(film);
            }

            Calendar.DisplayDate = filmbinding.PremiereDate;

            be = MainActors.GetBindingExpression(ListBox.ItemsSourceProperty);
            be.UpdateTarget();
            be = Cover.GetBindingExpression(Image.SourceProperty);
            be.UpdateTarget();
            be = Title.GetBindingExpression(TextBox.TextProperty);
            be.UpdateTarget();
            be = Director.GetBindingExpression(TextBox.TextProperty);
            be.UpdateTarget();
            be = Production.GetBindingExpression(TextBox.TextProperty);
            be.UpdateTarget();
            be = Review.GetBindingExpression(TextBox.TextProperty);
            be.UpdateTarget();
            be = Calendar.GetBindingExpression(Calendar.SelectedDateProperty);
            be.UpdateTarget();
            be = Category.GetBindingExpression(TextBox.TextProperty);
            be.UpdateTarget();
            be = Rating.GetBindingExpression(Slider.ValueProperty);
            be.UpdateTarget();
            be = Review.GetBindingExpression(TextBox.TextProperty);
            be.UpdateTarget();

            Title.Focus();
        }

        private void CANCEL_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            this.Close();
        }

        private void AddImage_Click(object sender, RoutedEventArgs e)
        {
            string oldPath;
            string newPath = "";

            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                oldPath = System.IO.Path.GetFullPath(openFileDialog.FileName);
                newPath = "covers\\" + System.IO.Path.GetFileName(openFileDialog.FileName);
                System.IO.File.Copy(oldPath, System.IO.Path.GetFullPath(newPath), true);
                filmbinding.UrlCover = newPath;
                //aktualizacja obrazu
                BindingExpression be = Cover.GetBindingExpression(Image.SourceProperty);
                be.UpdateTarget();
            }
        }
        private void DeleteImage_Click(object sender, RoutedEventArgs e)
        {
            filmbinding.UrlCover = "covers/default.jpg";
            BindingExpression be = Cover.GetBindingExpression(Image.SourceProperty);
            be.UpdateTarget();
        }

        public static RoutedUICommand COMMANDD
        {
            get { return COMMAND_VAR; }
        }

        private void COMMAND_Action(object sender, ExecutedRoutedEventArgs e)
        {
            if ((string)e.Parameter == "add")
            {
                ActorAdd window = new ActorAdd();
                
                if(window.ShowDialog()==true)
                {
                    filmbinding.MainActors.Add(String.Copy(  window.actor));
                }
            }
            if ((string)e.Parameter == "edit")
            {
                ActorAdd window = new ActorAdd();
                window.actor = filmbinding.MainActors[MainActors.SelectedIndex];
                if (window.ShowDialog() == true)

                    filmbinding.MainActors[MainActors.SelectedIndex] = String.Copy(window.actor);

            }
            if ((string)e.Parameter == "remove")
            {
                filmbinding.MainActors.RemoveAt(MainActors.SelectedIndex);
            }
        }
        private void COMMAND_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if ((string)e.Parameter == "add")
            {
                e.CanExecute = true;
            }
            if (((string)e.Parameter == "edit") || ((string)e.Parameter == "remove"))
            {
                if(MainActors.SelectedIndex>-1)
                    e.CanExecute = true;
                else
                    e.CanExecute = false;
            }
        }
        public static RoutedUICommand COMMANDD_OK
        {
            get { return COMMAND_OK_VAR; }
        }

        private void COMMAND_OK_Action(object sender, ExecutedRoutedEventArgs e)
        {
            film = new Film(filmbinding);

            DialogResult = IsNewFilm;
            this.Close();
        }
        private void COMMAND_OK_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (Validation.GetHasError(Title) == false && Validation.GetHasError(Director) == false)
            {
                e.CanExecute = true;
                return;
            }

            e.CanExecute = false;
            return;
        }
        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }
    }
}
