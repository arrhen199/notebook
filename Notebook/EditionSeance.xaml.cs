﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Notebook
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class EditionSeance : Window
    {
        public FilmsRepository films {get; set;}

        public Seance seance { get; set; }
        public Seance seancebinding { get; set; }

        private bool IsNewSeance = true;

        ListCollectionView filter;

         private static RoutedUICommand COMMAND_OK_VAR;

        public EditionSeance()
        {
            COMMAND_OK_VAR = new RoutedUICommand("n3", "n4", typeof(EditionSeance));
            InitializeComponent();


        }

        private bool FilmFilter(object item)
        {
            return ((item as Film).Title.Contains(Filter.Text));// && (item as Film).Director.Contains(TextBoxDirectorFilter.Text) && (item as Film).Category.Contains(TextBoxCategoryFilter.Text));
        }

        private void CANCEL_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void AddNewFilm_Click(object sender, RoutedEventArgs e)
        {
            EditionFilm dlg = new EditionFilm();

            if (dlg.ShowDialog() == true)
            {
                films.Films.Add(dlg.film);
            }
            ListSeance.Items.Refresh();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            BindingExpression be;
            if(seance==null)
            {
                seancebinding = new Seance();
                IsNewSeance = true;
            }
            else
            {
                seancebinding = new Seance(seance);
                ListSeance.SelectedIndex = films.Films.IndexOf(seance.Film);
                
                IsNewSeance = false;
                be = Date.GetBindingExpression(Calendar.SelectedDateProperty);
                be.UpdateTarget();
            }

            Date.DisplayDate = seancebinding.SeanceDate;
            //aktualizacje zródła wiązania

            be = Time.GetBindingExpression(TextBox.TextProperty);
            be.UpdateTarget();
            be = Duration.GetBindingExpression(TextBox.TextProperty);
            be.UpdateTarget();
            be = Location.GetBindingExpression(TextBox.TextProperty);
            be.UpdateTarget();
            be = IsChecked.GetBindingExpression(CheckBox.IsCheckedProperty);
            be.UpdateTarget();

            filter = (ListCollectionView)CollectionViewSource.GetDefaultView(ListSeance.ItemsSource);
            filter.Filter = FilmFilter; ;
        }
        public static RoutedUICommand COMMANDD_OK
        {
            get { return COMMAND_OK_VAR; }
        }
        private void COMMAND_OK_Action(object sender, ExecutedRoutedEventArgs e)
        {
            BindingExpression be;
            be = Date.GetBindingExpression(Calendar.SelectedDateProperty);
            be.UpdateSource();

            if (ListSeance.SelectedIndex > -1)

                seancebinding.Film = (Film)ListSeance.SelectedItem;

            if (IsNewSeance != false)
            {
                films.MySeances.Add(seancebinding);
            }

            seance = new Seance(seancebinding);

            DialogResult = true;
            this.Close();
        }
        private void COMMAND_OK_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (ListSeance.SelectedIndex>-1 && Validation.GetHasError(Location) == false && Validation.GetHasError(Duration) == false && Validation.GetHasError(Time) == false)
            {
                e.CanExecute = true;
                return;
            }

            e.CanExecute = false;
            return;
        }

        private void Filter_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(ListSeance.ItemsSource).Refresh();
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }
    }
}
