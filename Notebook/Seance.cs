﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml.Serialization;

namespace Notebook
{
    [Serializable]
    public class Seance : INotifyPropertyChanged, IDataErrorInfo
    {
        private Film film;
        public Film Film
        {
            get { return film; }
            set { film = value; OnPropertyChanged("Film"); }
        }

        private Boolean isWatched;
        public Boolean IsWatched
        {
            get { return isWatched; }
            set { isWatched = value; OnPropertyChanged("IsWatched"); }
        }

        private DateTime seanceDate;
        public DateTime SeanceDate
        {
            get { return seanceDate; }
            set { seanceDate = value; OnPropertyChanged("SeanceDate"); }
        }



        private String location;
        public String Location
        {
            get { return location; }
            set { location = value; OnPropertyChanged("Location"); }
        }

        private TimeSpan time;
        [XmlIgnore]
        public TimeSpan Time
        {
            get { return time; }
            set { time = value; OnPropertyChanged("Time"); }
        }
        [XmlElement("Time")]
        public long TimeXML
        {
            get { return Time.Ticks; }
            set { Time = new TimeSpan(value); }
        }

        private TimeSpan duration; // ?? nie wiem jeszcze jaki to będzie format
        [XmlIgnore]
        public TimeSpan Duration
        {
            get { return duration; }
            set { duration = value; OnPropertyChanged("Duration"); }
        }
        [XmlElement("Duration")]
        public long DurationXML
        {
            get { return Duration.Ticks; }
            set { Duration = new TimeSpan(value); }
        }

        public Seance(Seance parametr)
        {
           IsWatched=parametr.IsWatched;
           SeanceDate=parametr.SeanceDate;
           Time=parametr.Time;
           Location=String.Copy(parametr.Location);
           Duration = parametr.Duration;
           Film = parametr.Film;
        }
        public Seance()
        {
            seanceDate = DateTime.Now;
        }

        public Seance GetInstance
        {
            get
            {
                return this;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this,
                new PropertyChangedEventArgs(property));
        }

        public string Error
        {
            get { return String.Empty; }
        }

        public string this[string parametr]
        {
            get
            {
                string result = null;
                if (parametr == "Location")
                {
                    if (string.IsNullOrEmpty(Location))
                        result = " ";
                }


                return result;
            }
        }
    }
}
