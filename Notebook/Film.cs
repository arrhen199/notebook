﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notebook
{
    [Serializable]
    public class Film : INotifyPropertyChanged, IDataErrorInfo
    {
        public Film()
        {
            UrlCover = "covers/default.jpg";
            MainActors = new ObservableCollection<String>();
            Title = "";
            Director = "";
            Production = "";
            Review = "";
            Category = "";
            PremiereDate = DateTime.Now;
        }

        private String title;
        public String Title
        {
            get { return title; }
            set { title = value; OnPropertyChanged("Title"); }
        }

        private String urlCover;
        public String UrlCover
        {
            get { return urlCover; }
            set { urlCover = value; OnPropertyChanged("UrlCover"); }
        }

        public String Director { get; set; }
        public String Production { get; set; }
        public String Review { get; set; }
        public DateTime PremiereDate { get; set; }
        public String Category { get; set; }
        public int Rating { get; set; }
        public ObservableCollection<String> MainActors { get; set; }
        public Film GetInstance
        {
            get
            {
                return this;
            }
        }

        public override string ToString()
        {
            return Title;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this,
                new PropertyChangedEventArgs(property));
        }

        public Film(Film parametr)
        {
            UrlCover = String.Copy(parametr.UrlCover);
            MainActors = new ObservableCollection<String>(parametr.MainActors);
            Title=String.Copy(parametr.Title);
            Director = String.Copy(parametr.Director);
            Production = String.Copy(parametr.Production);
            Review = String.Copy(parametr.Review);
            PremiereDate =parametr.PremiereDate;
            Category = String.Copy(parametr.Category);
            Rating = parametr.Rating;
        }

        public string Error
        {
            get { return String.Empty; }
        }

        public string this[string parametr]
        {
            get
            {
                string result = null;
                if (parametr == "Title")
                {
                    if (string.IsNullOrEmpty(Title))
                        result = " ";
                }
                if (parametr == "Director")
                {
                    if (string.IsNullOrEmpty(Director))
                        result = " ";
                }
                if (parametr == "Production")
                {
                    if (string.IsNullOrEmpty(Production))
                        result = " ";
                }
                if (parametr == "Category")
                {
                    if (string.IsNullOrEmpty(Category))
                        result = " ";
                }
                return result;
            }
        }
    }
}
