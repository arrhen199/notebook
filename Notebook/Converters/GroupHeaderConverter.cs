﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Globalization;

namespace Notebook.Converters
{
    class GroupHeaderConverter :IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is DateTime)
            {                
                DateTime test = (DateTime)value;
                return test.ToString("dd MMMM, yyyy", new CultureInfo("pl-PL"));
            }
            else if (value is Boolean)
            {
                if ((bool)value) return "Obejrzane";
                return "Nieobejrzane";
            }
            else if (value is Int32)
            {
                return value.ToString();
            }
            return (string)value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
