﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace Notebook.Converters
{
    class FilmTemplateBackgroudConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value.ToString().Contains("Akcja") || value.ToString().Contains("Action"))
            {
               return new SolidColorBrush(Color.FromRgb(99, 22, 100));
            }
            else if (value.ToString().Contains("Komedia")||value.ToString().Contains("Comedy"))
            {                
                return new SolidColorBrush(Color.FromRgb(21,162,49));
            }
            else if (value.ToString().Contains("Science") || value.ToString().Contains("Fiction") || value.ToString().Contains("Sci-Fi"))
            {
                return new SolidColorBrush(Color.FromRgb(21, 96, 162));
            }
            else if (value.ToString().Contains("Thriller"))
            {
                return new SolidColorBrush(Color.FromRgb(99, 162,21));
            }
            else if (value.ToString().Contains("Dramat"))
            {
                return new SolidColorBrush(Color.FromRgb(99, 123, 136));
            }
            else if (value.ToString().Contains("Horror"))
            {
                return new SolidColorBrush(Color.FromRgb(162,91,21));
            }
            else
            {
                return new SolidColorBrush(Color.FromRgb(99, 223, 136));
            }

            //return (bool)value ? new SolidColorBrush(Color.FromRgb(99, 223, 136)) : new SolidColorBrush(Color.FromArgb(255, 255, 139, 0));
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
