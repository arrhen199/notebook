﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Notebook.Converters
{
    [ValueConversion(typeof(TimeSpan), typeof(string))]
    class TimeConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is TimeSpan)
            {
                TimeSpan test = (TimeSpan)value;
                string date = test.ToString(@"hh\:mm");
                return (date);
            }
            return string.Empty;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                var formats = @"hh\:mm\:ss";
                TimeSpan time = TimeSpan.ParseExact((string)value+":00", formats, culture);
                //MessageBox.Show(time.ToString(@"hh\:mm\:ss"));
                return time;
            }
            catch (FormatException)
            {
            }
            catch (OverflowException)
            {
            }
            return "";
        }
    }
}
