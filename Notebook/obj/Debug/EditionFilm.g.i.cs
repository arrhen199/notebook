﻿#pragma checksum "..\..\EditionFilm.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "422A7FA5929B9B0C3DDAA6678BA5CB24"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Notebook;
using Notebook.Converters;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Notebook {
    
    
    /// <summary>
    /// EditionFilm
    /// </summary>
    public partial class EditionFilm : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 166 "..\..\EditionFilm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Title;
        
        #line default
        #line hidden
        
        
        #line 169 "..\..\EditionFilm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Director;
        
        #line default
        #line hidden
        
        
        #line 172 "..\..\EditionFilm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Calendar Calendar;
        
        #line default
        #line hidden
        
        
        #line 175 "..\..\EditionFilm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Production;
        
        #line default
        #line hidden
        
        
        #line 184 "..\..\EditionFilm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Slider Rating;
        
        #line default
        #line hidden
        
        
        #line 189 "..\..\EditionFilm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Category;
        
        #line default
        #line hidden
        
        
        #line 204 "..\..\EditionFilm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image Cover;
        
        #line default
        #line hidden
        
        
        #line 206 "..\..\EditionFilm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button AddImage;
        
        #line default
        #line hidden
        
        
        #line 207 "..\..\EditionFilm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button DeleteImage;
        
        #line default
        #line hidden
        
        
        #line 234 "..\..\EditionFilm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox MainActors;
        
        #line default
        #line hidden
        
        
        #line 255 "..\..\EditionFilm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Review;
        
        #line default
        #line hidden
        
        
        #line 263 "..\..\EditionFilm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button OK;
        
        #line default
        #line hidden
        
        
        #line 264 "..\..\EditionFilm.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button CANCEL;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Notebook;component/editionfilm.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\EditionFilm.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 9 "..\..\EditionFilm.xaml"
            ((Notebook.EditionFilm)(target)).MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.Window_MouseDown);
            
            #line default
            #line hidden
            return;
            case 2:
            
            #line 11 "..\..\EditionFilm.xaml"
            ((System.Windows.Input.CommandBinding)(target)).Executed += new System.Windows.Input.ExecutedRoutedEventHandler(this.COMMAND_OK_Action);
            
            #line default
            #line hidden
            
            #line 11 "..\..\EditionFilm.xaml"
            ((System.Windows.Input.CommandBinding)(target)).CanExecute += new System.Windows.Input.CanExecuteRoutedEventHandler(this.COMMAND_OK_CanExecute);
            
            #line default
            #line hidden
            return;
            case 3:
            
            #line 12 "..\..\EditionFilm.xaml"
            ((System.Windows.Input.CommandBinding)(target)).Executed += new System.Windows.Input.ExecutedRoutedEventHandler(this.COMMAND_Action);
            
            #line default
            #line hidden
            
            #line 12 "..\..\EditionFilm.xaml"
            ((System.Windows.Input.CommandBinding)(target)).CanExecute += new System.Windows.Input.CanExecuteRoutedEventHandler(this.COMMAND_CanExecute);
            
            #line default
            #line hidden
            return;
            case 4:
            this.Title = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.Director = ((System.Windows.Controls.TextBox)(target));
            return;
            case 6:
            this.Calendar = ((System.Windows.Controls.Calendar)(target));
            return;
            case 7:
            this.Production = ((System.Windows.Controls.TextBox)(target));
            return;
            case 8:
            this.Rating = ((System.Windows.Controls.Slider)(target));
            return;
            case 9:
            this.Category = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            
            #line 194 "..\..\EditionFilm.xaml"
            ((System.Windows.Controls.Grid)(target)).Loaded += new System.Windows.RoutedEventHandler(this.Loadedd);
            
            #line default
            #line hidden
            return;
            case 11:
            this.Cover = ((System.Windows.Controls.Image)(target));
            return;
            case 12:
            this.AddImage = ((System.Windows.Controls.Button)(target));
            
            #line 206 "..\..\EditionFilm.xaml"
            this.AddImage.Click += new System.Windows.RoutedEventHandler(this.AddImage_Click);
            
            #line default
            #line hidden
            return;
            case 13:
            this.DeleteImage = ((System.Windows.Controls.Button)(target));
            
            #line 207 "..\..\EditionFilm.xaml"
            this.DeleteImage.Click += new System.Windows.RoutedEventHandler(this.DeleteImage_Click);
            
            #line default
            #line hidden
            return;
            case 14:
            this.MainActors = ((System.Windows.Controls.ListBox)(target));
            return;
            case 15:
            this.Review = ((System.Windows.Controls.TextBox)(target));
            return;
            case 16:
            this.OK = ((System.Windows.Controls.Button)(target));
            return;
            case 17:
            this.CANCEL = ((System.Windows.Controls.Button)(target));
            
            #line 264 "..\..\EditionFilm.xaml"
            this.CANCEL.Click += new System.Windows.RoutedEventHandler(this.CANCEL_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

