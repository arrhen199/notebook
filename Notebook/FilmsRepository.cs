﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notebook
{
    [Serializable]
    public class FilmsRepository
    {
        //kolekcja wszystkich filmów(bez zaplanowanych)
        public Collection<Film> Films { get; set; }
        //kolekcja zaplanowanych filmów
        public Collection<Seance> MySeances { get; set; }

        private Collection<Film> _premieres;
        public Collection<Film> Premieres
        {
            get
            {
                if (_premieres == null)
                {
                    _premieres = new Collection<Film>();
                    foreach (var item in Films)
                    {
                        if (item.PremiereDate > DateTime.Now)
                        {
                            _premieres.Add(item);
                        }
                    }
                }
                else
                {
                    _premieres.Clear();
                    foreach (var item in Films)
                    {
                        if (item.PremiereDate > DateTime.Now)
                        {
                            _premieres.Add(item);
                        }
                    }
                }
                return _premieres;
            }
        }

        public FilmsRepository()
        {
            Films = new ObservableCollection<Film>();
            MySeances = new ObservableCollection<Seance>();
        }
        public void CorrectAfterLoaded()
        {
            for (int s = 0; s < MySeances.Count; s++)
            {
                for (int i = 0; i < Films.Count; i++)
                {
                    if (MySeances[s].Film.Title == Films[i].Title)
                        MySeances[s].Film = Films[i];
                }
            }

        }

    }
}
