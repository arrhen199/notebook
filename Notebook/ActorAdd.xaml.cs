﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Notebook
{
    /// <summary>
    /// Interaction logic for ActorAdd.xaml
    /// </summary>
    public partial class ActorAdd : Window
    {
        public string actor  { get; set; }

        private static RoutedUICommand COMMAND_VAR;

        public ActorAdd()
        {
            COMMAND_VAR = new RoutedUICommand("n1", "n2", typeof(ActorAdd));
            InitializeComponent();
        }

        public static RoutedUICommand COMMANDD
        {
            get { return COMMAND_VAR; }
        }

        private void COMMAND_Action(object sender, ExecutedRoutedEventArgs e)
        {
            if ((string)e.Parameter == "ok")
            {
                DialogResult = true;
            }
            if ((string)e.Parameter == "cancel")
            {
                DialogResult = false;
            }
            this.Close();
        }
        private void COMMAND_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if ((string)e.Parameter == "ok")
            {
                if (Value.Text.Length > 0)
                    e.CanExecute = true;
                else
                    e.CanExecute = false;
            }
            if ((string)e.Parameter == "cancel") 
            {
                e.CanExecute = true;
            }
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }
    }
}
