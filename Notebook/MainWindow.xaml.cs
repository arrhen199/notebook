﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Serialization;

namespace Notebook
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        FilmsRepository films;
        ListCollectionView FilmsCollectionViewPremiere;
        ListCollectionView FilmsCollectionViewSeance;
        ListCollectionView FilmsCollectionViewFilms;
        public Collection<Film> FilmPremieres { get; set; }

        private static RoutedUICommand DeleteCommand;

        public MainWindow()
        {
            DeleteCommand = new RoutedUICommand("n1", "n2", typeof(MainWindow));

            films = new FilmsRepository();

            try
            {
                TextReader tr = new StreamReader("DataBase.xml");
                XmlSerializer sr = new XmlSerializer(typeof(FilmsRepository));
                films = (FilmsRepository)sr.Deserialize(tr);
                tr.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show("Nie odnaleniono pliku z danymi. Utworzono nową bazę dnych.");
            }

            InitializeComponent();

            ListBoxFilmPremiere.ItemsSource = films.Films;
            FilmsCollectionViewPremiere = (ListCollectionView)CollectionViewSource.GetDefaultView(ListBoxFilmPremiere.ItemsSource);
            FilmsCollectionViewPremiere.Filter = PremiereFilter;

            ListBoxFilmsSeance.ItemsSource = films.MySeances;
            FilmsCollectionViewSeance = (ListCollectionView)CollectionViewSource.GetDefaultView(ListBoxFilmsSeance.ItemsSource);
            FilmsCollectionViewSeance.Filter = SeanceFilter;

            ListBoxAllFilms.ItemsSource = films.Films;
            FilmsCollectionViewFilms = (ListCollectionView)CollectionViewSource.GetDefaultView(ListBoxAllFilms.ItemsSource);
            FilmsCollectionViewFilms.Filter = FilmFilter;
            films.CorrectAfterLoaded();


        }
        private bool PremiereFilter(object item)
        {
            return ((item as Film).Title.Contains(TextBoxTitleFilter.Text) && (item as Film).Director.Contains(TextBoxDirectorFilter.Text) && (item as Film).Category.Contains(TextBoxCategoryFilter.Text));
        }
        private bool FilmFilter(object item)
        {
            return ((item as Film).Title.Contains(TextBoxFilmTitleFilter.Text) && (item as Film).Director.Contains(TextBoxFilmDirectorFilter.Text) && (item as Film).Category.Contains(TextBoxFilmCategoryFilter.Text));
        }

        private void TextBoxFilmFilter_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(ListBoxAllFilms.ItemsSource).Refresh();
        }

        private void TextBoxPremiereFilter_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(ListBoxFilmPremiere.ItemsSource).Refresh();
        }

        private void ComboBoxPremiereGroup_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox comboBox = sender as ComboBox;
            ComboBoxItem item = comboBox.SelectedItem as ComboBoxItem;
            if (FilmsCollectionViewPremiere != null)
            {
                FilmsCollectionViewPremiere.GroupDescriptions.Clear();
                if ((string)item.Tag != String.Empty)
                    FilmsCollectionViewPremiere.GroupDescriptions.Add(new PropertyGroupDescription((string)item.Tag));
            }
        }
        private void ComboBoxFilmGroup_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox comboBox = sender as ComboBox;
            ComboBoxItem item = comboBox.SelectedItem as ComboBoxItem;
            if (FilmsCollectionViewFilms != null)
            {
                FilmsCollectionViewFilms.GroupDescriptions.Clear();
                if ((string)item.Tag != String.Empty)
                    FilmsCollectionViewFilms.GroupDescriptions.Add(new PropertyGroupDescription((string)item.Tag));
            }
        }
        private void ComboBoxPremiereSort_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox comboBox = sender as ComboBox;
            ComboBoxItem item = comboBox.SelectedItem as ComboBoxItem;
            if (FilmsCollectionViewPremiere != null)
            {
                FilmsCollectionViewPremiere.SortDescriptions.Clear();
                if ((string)item.Tag != String.Empty)
                    FilmsCollectionViewPremiere.SortDescriptions.Add(new SortDescription((string)item.Tag, ListSortDirection.Ascending));
            }
        }
        private void ComboBoxFilmSort_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox comboBox = sender as ComboBox;
            ComboBoxItem item = comboBox.SelectedItem as ComboBoxItem;
            if (FilmsCollectionViewFilms != null)
            {
                FilmsCollectionViewFilms.SortDescriptions.Clear();
                if ((string)item.Tag != String.Empty && (string)item.Tag == "Rating")
                    FilmsCollectionViewFilms.SortDescriptions.Add(new SortDescription((string)item.Tag, ListSortDirection.Descending));
                else if ((string)item.Tag != String.Empty)
                    FilmsCollectionViewFilms.SortDescriptions.Add(new SortDescription((string)item.Tag, ListSortDirection.Ascending));
            }
        }

        private void ComboBoxSeanceSort_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox comboBox = sender as ComboBox;
            ComboBoxItem item = comboBox.SelectedItem as ComboBoxItem;
            if (FilmsCollectionViewSeance != null)
            {
                FilmsCollectionViewSeance.SortDescriptions.Clear();
                if ((string)item.Tag != String.Empty)
                    FilmsCollectionViewSeance.SortDescriptions.Add(new SortDescription((string)item.Tag, ListSortDirection.Ascending));
            }
        }
        private void ComboBoxSeanceGroup_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox comboBox = sender as ComboBox;
            ComboBoxItem item = comboBox.SelectedItem as ComboBoxItem;
            if (FilmsCollectionViewSeance != null)
            {
                FilmsCollectionViewSeance.GroupDescriptions.Clear();
                if ((string)item.Tag != String.Empty)
                    FilmsCollectionViewSeance.GroupDescriptions.Add(new PropertyGroupDescription((string)item.Tag)); // trzeba jakoś zrobić żeby porównywało wartośći z filmu
            }
        }
        private bool SeanceFilter(object item)
        {
            return ((item as Seance).Location.Contains(TextBoxLocationFilterSeance.Text) && (item as Seance).Film.Director.Contains(TextBoxDirectorFilterSeance.Text) && (item as Seance).Film.Category.Contains(TextBoxCategoryFilterSeance.Text));
        }
        private void TextBoxSeanceFilter_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(ListBoxFilmsSeance.ItemsSource).Refresh();
        }

        public static RoutedUICommand DeleteFilmCommand
        {
            get { return DeleteCommand; }
        }

        private void DeleteCommand_Action(object sender, ExecutedRoutedEventArgs e)
        {
            if (e.Parameter.GetType() == typeof(Film)) films.Films.Remove((Film)e.Parameter);
            else if (e.Parameter.GetType() == typeof(Seance)) films.MySeances.Remove((Seance)e.Parameter);

        }
        private void DeleteCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            try
            {
                TextWriter tw = new StreamWriter("DataBase.xml");
                XmlSerializer sr = new XmlSerializer(typeof(FilmsRepository));
                sr.Serialize(tw, films);
                tw.Close();
            }
            catch (Exception)
            {
                MessageBox.Show("Nie udało się zapisać pliku. Wprowadzone dane zostały utracone");
            }
        }
        void TabItemMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Label item = sender as Label;

            if (String.Compare(item.Content.ToString(), "Moje Seanse") == 0)
            {
                EditionSeance window = new EditionSeance();
                window.films = films;
                window.ShowDialog();
                ListBoxFilmPremiere.Items.Refresh();
            }
            else//edycja filmu
            {
                EditionFilm dlg = new EditionFilm();

                if (dlg.ShowDialog() == true)
                {
                    films.Films.Add(dlg.film);
                }
                ListBoxFilmPremiere.Items.Refresh();
            }
        }

        private void ListBoxPremiereFilms_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (ListBoxFilmPremiere.SelectedIndex >= 0)
            {
                EditionFilm dlg = new EditionFilm();
                dlg.film = (Film)ListBoxFilmPremiere.SelectedItem;

                dlg.ShowDialog();

                films.Films[ListBoxFilmPremiere.SelectedIndex] = dlg.film;
                ListBoxFilmPremiere.Items.Refresh();
            }
            else
            {
                return;
            }
        }
        private void ListBoxAllFilms_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

            if (ListBoxAllFilms.SelectedIndex > 0)
            {
                EditionFilm dlg = new EditionFilm();
                dlg.film = (Film)ListBoxAllFilms.SelectedItem;

                dlg.ShowDialog();

                films.Films[ListBoxAllFilms.SelectedIndex] = dlg.film;
                ListBoxAllFilms.Items.Refresh();
            }
            else
            {
                return;
            }



        }

        private void ListBoxFilmsSeance_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            EditionSeance window = new EditionSeance();

            window.films = films;

            window.seance = films.MySeances[ListBoxFilmsSeance.SelectedIndex];
            if (window.ShowDialog() == true)
                films.MySeances[ListBoxFilmsSeance.SelectedIndex] = window.seance;
            ListBoxFilmPremiere.Items.Refresh();
        }

        private void ButtonOFf2_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                TextWriter tw = new StreamWriter("DataBase.xml");
                XmlSerializer sr = new XmlSerializer(typeof(FilmsRepository));
                sr.Serialize(tw, films);
                tw.Close();
            }
            catch (Exception)
            {
                MessageBox.Show("Nie udało się zapisać pliku. Wprowadzone dane zostały utracone");
            }
            this.Close();
        }

        private void ListBoxFilms_PreviewMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (ListBoxAllFilms.SelectedIndex > -1)
            {
                DocumentFilm document = new DocumentFilm();
                document.film = films.Films[ListBoxAllFilms.SelectedIndex];
                document.ShowDialog();
            }
        }
        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }
    }
}
