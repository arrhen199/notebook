﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Notebook
{
    /// <summary>
    /// Interaction logic for Stars.xaml
    /// </summary>
    public partial class Stars : UserControl
    {
        //  public int rating {get; set;}
        public static readonly DependencyProperty SourceProperty = DependencyProperty.Register("rating", typeof(int), typeof(Stars));

        public Stars()
        {
            InitializeComponent();
        }

        public int rating
        {
            get { return (int)GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            string path = "";

            if (rating == 1)
                path = "stars/star1.jpg";
            if (rating == 2)
                path = "stars/star2.jpg";
            if (rating == 3)
                path = "stars/star3.jpg";
            if (rating == 4)
                path = "stars/star4.jpg";
            if (rating == 5)
                path = "stars/star5.jpg";
            try
            {
                s.Source = new BitmapImage(new Uri((string)System.IO.Path.GetFullPath(path)));
            }
            catch
            {
                s.Source = new BitmapImage();
            }
        }
    }
}
